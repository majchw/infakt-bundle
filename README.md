# README #

Paczka integrująca wymianę danych z fakturownią Infakt.pl przy wykorzystaniu [Infakt API Client](https://bitbucket.org/majchw/infakt-client-php/src/master/)

## Instalacja

###  1: Przygotowanie konfiguracji

Dodaj odpowiednie wpisy konfiguracyjne do pliku .env:
```
### Konfiguracja INFAKT
INFAKT_APIKEY="<tu wpisz klucz API>"
INFAKT_ENDPOINT="https://api.infakt.pl/api"
INFAKT_VERSION="v3"
```

###  2: Podpięcie bundla do projektu
W głównym katalogu z projektem bazującym na swp i zainstaluj bundle z wykorzystaniem composera:
```
composer require x-one/infakt-bundle
```

### 3: Wpis w config/bundles.php:

Należy zweryfikować obecność i w razie potrzeby dodać wpis aktywujący nasz bundle w pliku config/bundles.php
```
XOne\Bundle\InfaktBundle\XOneInfaktBundle::class => ['all' => true]
```

## Wykorzystanie
Po zainstalowaniu Infakt-bundle można wykorzystać go w dowolnym miejscu w projekcie poprzez wstrzykniecie zależności, np.

    public  function __construct(Infakt $infakt) {
    	$this->infakt = $infakt;
    }

Dostępne opcje związane z obsługą fakturowni Infakt znajdują się w readme [Infakt API Client](https://bitbucket.org/majchw/infakt-client-php/src/master/) oraz dokumentacją Api Infakt
